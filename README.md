
<p align="center">
  <a href="https://www.xerpa.com.br" target="blank"><img src="https://assets.website-files.com/5e84c6bfaf8c1736e76d1782/5f04b554c26b6d0d68a29fb4_Logo%20Xerpay%20-%20Grana.png" width="800" alt="JusBrasil Logo" /></a>
</p>
<p align="center">Solução de <a href="https://gitlab.com/elaineprata" target="blank">Elaine Prata</a> para o <a href="https://docs.google.com/document/d/1vJsir72GlnCbilZ-qYV2pm11MbzHohIMtjas1SiZB2w/edit#" target="blank">desafio</a> da <a href="https://www.xerpa.com.br" target="blank">Xerpa</a>.
    <p align="center">
    


## Como iniciar a aplicação

- Pela IDE (IntelliJ, Eclipse, STS)

Importe o projeto como projeto gradle.
Na classe, Application, clique com o botão direito e selecione Run.

- Pelo Jar

```bash
java -jar exploring-mars-1.0-SNAPSHOT.jar
```

## Entrada de Dados

Ao executar a aplicação, os dados serão requisitados no terminal conforme o exemplo:

```
Enter Plateau limits (X Y): 
4 4
Enter probe position (X Y D): 
3 2 S
Enter probe movements (ex.: LMRM): 
LMLM
Enter probe position (X Y D): 

```

Para terminar a entrada de dados, basta pressionar enter na entrada de uma nova sonda (`probe position`).

Exemplo de saída completo:

```
Enter Plateau limits (X Y): 
5 5
Enter probe position (X Y D): 
1 2 N
Enter probe movements (ex.: LMRM): 
LMLMLMLMM
Enter probe position (X Y D): 
3 3 E
Enter probe movements (ex.: LMRM): 
MMRMMRMRRM
Enter probe position (X Y D): 


1 3 N
5 1 E
```


## Configuração

O arquivo application.properties contém algumas configurações que influenciam no comportamento da aplicação:

```
###############################################################################
# APPLICATION CONFIG
###############################################################################

collision.enabled=false
stop.after.reach.plateau.limit=true
 
```

- collision.enabled
Indica se o posicionamento de duas ou mais sondas na mesma coordenada pode ser ignorado.
Quando setado como `false` indica que durante a movimentação uma sonda pode passar pela mesma coordenada em que outra sonda se encontra.
Caso seja `true` o movimento que levaria a sonda para o mesmo posicionamento de outra é ignorado e a partir dai a sonda nao e mais movimentada.
Valor `default`:  `false`

- stop.after.reach.plateau.limit
Indica se ao atingir os limites do planalto e indicar movimento que ultrapasse esses limites, a sonda deve parar ou apenas os movimentos que a levam para fora do limite do planalto devem ser ignorados.
Quando setado como `true` o movimento que levaria a sonda para fora do planalto e todos os movimentos subsequentes sao ignorados.
Quando setado como `false` apenas o movimento que levaria a sonda para fora do planalto e ignorado e os movimentos subsequentes podem continuar.


## Entregáveis

A pasta deliverables contém alguns entregáveis:
 
- <a href="https://gitlab.com/elaineprata/exploring-mars/-/tree/master/deliverables/libs" target="blank">jar</a>

- relatório de <a href="https://gitlab.com/elaineprata/exploring-mars/-/tree/master/deliverables/test" target="blank">testes</a>

- relatório de <a href="https://gitlab.com/elaineprata/exploring-mars/-/tree/master/deliverables/coverage" target="blank">cobertura</a> de código


## Melhorias

- Tratamento de erros

- Cobertura de código

- etc