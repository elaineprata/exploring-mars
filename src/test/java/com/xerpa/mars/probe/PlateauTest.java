package com.xerpa.mars.probe;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PlateauTest {

    @Test
    public void isOffLimits() {
        Plateau plateau = new Plateau(new Point(5, 5));

        Assert.assertFalse(plateau.isOffLimits(new Point(0, 0)));
        Assert.assertFalse(plateau.isOffLimits(new Point(5, 5)));

        Assert.assertTrue(plateau.isOffLimits(new Point(6, 0)));
        Assert.assertTrue(plateau.isOffLimits(new Point(0, 6)));


        plateau = new Plateau(new Point(2, 2));

        Assert.assertFalse(plateau.isOffLimits(new Point(0, 0)));
        Assert.assertFalse(plateau.isOffLimits(new Point(0, 1)));
        Assert.assertFalse(plateau.isOffLimits(new Point(0, 2)));

        Assert.assertFalse(plateau.isOffLimits(new Point(1, 0)));
        Assert.assertFalse(plateau.isOffLimits(new Point(1, 1)));
        Assert.assertFalse(plateau.isOffLimits(new Point(1, 2)));

        Assert.assertFalse(plateau.isOffLimits(new Point(2, 0)));
        Assert.assertFalse(plateau.isOffLimits(new Point(2, 1)));
        Assert.assertFalse(plateau.isOffLimits(new Point(2, 2)));

        Assert.assertTrue(plateau.isOffLimits(new Point(6, 0)));
        Assert.assertTrue(plateau.isOffLimits(new Point(-1, 0)));
        Assert.assertTrue(plateau.isOffLimits(new Point(0, -1)));
        Assert.assertTrue(plateau.isOffLimits(new Point(-1, -1)));
    }

    @Test
    public void isThereCollision() throws Exception {

        Plateau plateau = new Plateau(new Point(5, 5));

        Probe p01 = new Probe(1, new ProbePosition(new Point(0, 0), ProbeDirection.N), null);
        Probe p02 = new Probe(2, new ProbePosition(new Point(0, 1), ProbeDirection.N), null);
        Probe p03 = new Probe(3, new ProbePosition(new Point(1, 1), ProbeDirection.N), null);
        Probe p04 = new Probe(4, new ProbePosition(new Point(1, 1), ProbeDirection.N), null);

        List<Probe> probeList = new ArrayList<>();
        probeList.add(p01);
        probeList.add(p02);
        probeList.add(p03);
        probeList.add(p04);

        plateau.setProbeList(probeList);

        Assert.assertFalse(plateau.isThereCollision(1, new Point(0, 0)));
        Assert.assertTrue(plateau.isThereCollision(2, new Point(0, 0)));
        Assert.assertTrue(plateau.isThereCollision(1, new Point(1, 1)));
    }
}
