package com.xerpa.mars.probe;

import org.junit.Assert;
import org.junit.Test;

public class ProbePositionTest {

    @Test
    public void testCalcNewPosition_M() throws Exception {
        // N
        ProbePosition position = new ProbePosition(new Point(0, 0), ProbeDirection.N);
        ProbePosition newPosition01 = position.calcNewPosition(ProbeMovement.M);

        Assert.assertEquals(new Point(0, 1), newPosition01.getPoint());
        Assert.assertEquals(ProbeDirection.N, newPosition01.getDirection());

        // E
        ProbePosition position02 = new ProbePosition(new Point(0, 0), ProbeDirection.E);
        ProbePosition newPosition02 = position02.calcNewPosition(ProbeMovement.M);

        Assert.assertEquals(new Point(1, 0), newPosition02.getPoint());
        Assert.assertEquals(ProbeDirection.E, newPosition02.getDirection());

        // S
        ProbePosition position03 = new ProbePosition(new Point(0, 1), ProbeDirection.S);
        ProbePosition newPosition03 = position03.calcNewPosition(ProbeMovement.M);

        Assert.assertEquals(new Point(0, 0), newPosition03.getPoint());
        Assert.assertEquals(ProbeDirection.S, newPosition03.getDirection());

        // W
        ProbePosition position04 = new ProbePosition(new Point(1, 0), ProbeDirection.W);
        ProbePosition newPosition04 = position04.calcNewPosition(ProbeMovement.M);

        Assert.assertEquals(new Point(0, 0), newPosition04.getPoint());
        Assert.assertEquals(ProbeDirection.W, newPosition04.getDirection());
    }

    @Test
    public void testCalcNewPosition_R() throws Exception {
        // N
        ProbePosition position = new ProbePosition(new Point(0, 0), ProbeDirection.N);
        ProbePosition newPosition01 = position.calcNewPosition(ProbeMovement.R);

        Assert.assertEquals(new Point(0, 0), newPosition01.getPoint());
        Assert.assertEquals(ProbeDirection.E, newPosition01.getDirection());

        // E
        ProbePosition position02 = new ProbePosition(new Point(0, 0), ProbeDirection.E);
        ProbePosition newPosition02 = position02.calcNewPosition(ProbeMovement.R);

        Assert.assertEquals(new Point(0, 0), newPosition02.getPoint());
        Assert.assertEquals(ProbeDirection.S, newPosition02.getDirection());

        // S
        ProbePosition position03 = new ProbePosition(new Point(0, 0), ProbeDirection.S);
        ProbePosition newPosition03 = position03.calcNewPosition(ProbeMovement.R);

        Assert.assertEquals(new Point(0, 0), newPosition03.getPoint());
        Assert.assertEquals(ProbeDirection.W, newPosition03.getDirection());

        // W
        ProbePosition position04 = new ProbePosition(new Point(0, 0), ProbeDirection.W);
        ProbePosition newPosition04 = position04.calcNewPosition(ProbeMovement.R);

        Assert.assertEquals(new Point(0, 0), newPosition04.getPoint());
        Assert.assertEquals(ProbeDirection.N, newPosition04.getDirection());
    }

    @Test
    public void testCalcNewPosition_L() throws Exception {

        // N
        ProbePosition position = new ProbePosition(new Point(0, 0), ProbeDirection.N);
        ProbePosition newPosition01 = position.calcNewPosition(ProbeMovement.L);

        Assert.assertEquals(new Point(0, 0), newPosition01.getPoint());
        Assert.assertEquals(ProbeDirection.W, newPosition01.getDirection());

        // E
        ProbePosition position02 = new ProbePosition(new Point(0, 0), ProbeDirection.E);
        ProbePosition newPosition02 = position02.calcNewPosition(ProbeMovement.L);

        Assert.assertEquals(new Point(0, 0), newPosition02.getPoint());
        Assert.assertEquals(ProbeDirection.N, newPosition02.getDirection());

        // S
        ProbePosition position03 = new ProbePosition(new Point(0, 0), ProbeDirection.S);
        ProbePosition newPosition03 = position03.calcNewPosition(ProbeMovement.L);

        Assert.assertEquals(new Point(0, 0), newPosition03.getPoint());
        Assert.assertEquals(ProbeDirection.E, newPosition03.getDirection());

        // W
        ProbePosition position04 = new ProbePosition(new Point(0, 0), ProbeDirection.W);
        ProbePosition newPosition04 = position04.calcNewPosition(ProbeMovement.L);

        Assert.assertEquals(new Point(0, 0), newPosition04.getPoint());
        Assert.assertEquals(ProbeDirection.S, newPosition04.getDirection());
    }
}