package com.xerpa.mars.controller;

import com.xerpa.mars.exception.InvalidInitialProbePositionsException;
import com.xerpa.mars.input.parser.PlateauParser;
import com.xerpa.mars.input.parser.ProbeMovementsParser;
import com.xerpa.mars.input.parser.ProbePositionParser;
import com.xerpa.mars.probe.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ProbeControllerTest {

    @Test
    public void validateInitialProbePositions_collisionEnabled_false() throws Exception {
        ProbePosition pp1 = ProbePositionParser.parse("0 0 W");
        List<ProbeMovement> pmList1 = ProbeMovementsParser.parseMovements("MRM");
        Probe probe1 = new Probe(1, pp1, pmList1);

        ProbePosition pp2 = ProbePositionParser.parse("0 0 N");
        List<ProbeMovement> pmList2 = ProbeMovementsParser.parseMovements("M");
        Probe probe2 = new Probe(2, pp2, pmList2);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe1);
        plateau.addProbe(probe2);

        ProbeController controller = new ProbeController(plateau, false, true);
        controller.validateInitialProbePositions();
    }

    @Test(expected = InvalidInitialProbePositionsException.class)
    public void validateInitialProbePositions_collisionEnabled_true() throws Exception {
        ProbePosition pp1 = ProbePositionParser.parse("0 0 W");
        List<ProbeMovement> pmList1 = ProbeMovementsParser.parseMovements("MRM");
        Probe probe1 = new Probe(1, pp1, pmList1);

        ProbePosition pp2 = ProbePositionParser.parse("0 0 N");
        List<ProbeMovement> pmList2 = ProbeMovementsParser.parseMovements("M");
        Probe probe2 = new Probe(2, pp2, pmList2);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe1);
        plateau.addProbe(probe2);

        ProbeController controller = new ProbeController(plateau, true, true);
        controller.validateInitialProbePositions();
    }

    @Test(expected = InvalidInitialProbePositionsException.class)
    public void validateInitialProbePositions_offLimits() throws Exception {
        ProbePosition pp1 = ProbePositionParser.parse("6 0 W");
        List<ProbeMovement> pmList1 = ProbeMovementsParser.parseMovements("MRM");
        Probe probe1 = new Probe(1, pp1, pmList1);

        ProbePosition pp2 = ProbePositionParser.parse("0 0 N");
        List<ProbeMovement> pmList2 = ProbeMovementsParser.parseMovements("M");
        Probe probe2 = new Probe(2, pp2, pmList2);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe1);
        plateau.addProbe(probe2);

        ProbeController controller = new ProbeController(plateau, false, true);
        controller.validateInitialProbePositions();
    }

    @Test
    public void moveProbe() throws Exception {

        List<ProbeMovement> list = ProbeMovementsParser.parseMovements("MLMLMLM");
        Probe probe = new Probe(1, new ProbePosition(new Point(0, 0), ProbeDirection.E), list);

        Plateau plateau = new Plateau(new Point(5, 5));
        plateau.addProbe(probe);

        ProbeController controller = new ProbeController(plateau, false, true);
        controller.moveProbe(probe);

        Assert.assertEquals(new ProbePosition(new Point(0, 0), ProbeDirection.S), probe.getPosition());
    }

    @Test
    public void moveProbe_stopAfterReachLimits_true() throws Exception {

        ProbePosition pp = ProbePositionParser.parse("5 4 N");
        List<ProbeMovement> pmList = ProbeMovementsParser.parseMovements("MMLM");
        Probe probe = new Probe(1, pp, pmList);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe);

        ProbeController controller = new ProbeController(plateau, false, true);
        controller.moveProbe(probe);

        Assert.assertEquals(new ProbePosition(new Point(5, 5), ProbeDirection.N), probe.getPosition());
    }

    @Test
    public void moveProbe_stopAfterReachLimits_false() throws Exception {

        ProbePosition pp = ProbePositionParser.parse("5 4 N");
        List<ProbeMovement> pmList = ProbeMovementsParser.parseMovements("MMLM");
        Probe probe = new Probe(1, pp, pmList);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe);

        ProbeController controller = new ProbeController(plateau, false, false);
        controller.moveProbe(probe);

        Assert.assertEquals(new ProbePosition(new Point(4, 5), ProbeDirection.W), probe.getPosition());
    }

    @Test
    public void moveProbe_collisionEnabled_false() throws Exception {

        ProbePosition pp1 = ProbePositionParser.parse("1 0 W");
        List<ProbeMovement> pmList1 = ProbeMovementsParser.parseMovements("MRM");
        Probe probe1 = new Probe(1, pp1, pmList1);

        ProbePosition pp2 = ProbePositionParser.parse("0 0 N");
        List<ProbeMovement> pmList2 = ProbeMovementsParser.parseMovements("M");
        Probe probe2 = new Probe(2, pp2, pmList2);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe1);
        plateau.addProbe(probe2);

        ProbeController controller = new ProbeController(plateau, false, true);
        controller.moveProbe(probe1);

        Assert.assertEquals(new ProbePosition(new Point(0, 1), ProbeDirection.N), probe1.getPosition());
    }

    @Test
    public void moveProbe_collisionEnabled_true() throws Exception {

        ProbePosition pp1 = ProbePositionParser.parse("1 0 W");
        List<ProbeMovement> pmList1 = ProbeMovementsParser.parseMovements("MRM");
        Probe probe1 = new Probe(1, pp1, pmList1);

        ProbePosition pp2 = ProbePositionParser.parse("0 0 N");
        List<ProbeMovement> pmList2 = ProbeMovementsParser.parseMovements("M");
        Probe probe2 = new Probe(2, pp2, pmList2);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe1);
        plateau.addProbe(probe2);

        ProbeController controller = new ProbeController(plateau, true, true);
        controller.moveProbe(probe1);

        Assert.assertEquals(new ProbePosition(new Point(1, 0), ProbeDirection.W), probe1.getPosition());
    }

    @Test
    public void moveProbes() throws Exception {
        /*
        5 5
        1 2 N
        LMLMLMLMM
        3 3 E
        MMRMMRMRRM
         */

        ProbePosition pp1 = ProbePositionParser.parse("1 2 N");
        List<ProbeMovement> pmList1 = ProbeMovementsParser.parseMovements("LMLMLMLMM");
        Probe probe1 = new Probe(1, pp1, pmList1);

        ProbePosition pp2 = ProbePositionParser.parse("3 3 E");
        List<ProbeMovement> pmList2 = ProbeMovementsParser.parseMovements("MMRMMRMRRM");
        Probe probe2 = new Probe(2, pp2, pmList2);

        Plateau plateau = PlateauParser.parse("5 5");
        plateau.addProbe(probe1);
        plateau.addProbe(probe2);

        ProbeController controller = new ProbeController(plateau, false, true);
        controller.moveProbes();

        // 1 3 N
        // 5 1 E
        Assert.assertEquals(new ProbePosition(new Point(1, 3), ProbeDirection.N), probe1.getPosition());
        Assert.assertEquals(new ProbePosition(new Point(5, 1), ProbeDirection.E), probe2.getPosition());
    }
}
