package com.xerpa.mars;

import com.xerpa.mars.controller.ProbeControllerTest;
import com.xerpa.mars.input.PlateauInputReaderTest;
import com.xerpa.mars.input.ProbeMovementsInputReaderTest;
import com.xerpa.mars.input.ProbePositionInputReaderTest;
import com.xerpa.mars.input.parser.InputValidatorTest;
import com.xerpa.mars.probe.PlateauTest;
import com.xerpa.mars.probe.ProbePositionTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        InputValidatorTest.class,
        PlateauInputReaderTest.class,
        ProbeMovementsInputReaderTest.class,
        ProbePositionInputReaderTest.class,
        PlateauTest.class,
        ProbePositionTest.class,
        ProbeControllerTest.class
})
public class JUnitSuite {
}