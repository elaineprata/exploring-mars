package com.xerpa.mars.input;

import com.xerpa.mars.probe.ProbeMovement;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Scanner;

public class ProbeMovementsInputReaderTest {

    @Test
    public void createObject() throws Exception {
        Scanner scanner = new Scanner("MLMR");

        ProbeMovementsInputReader pmReader = new ProbeMovementsInputReader(scanner);
        List<ProbeMovement> list = pmReader.read();

        Assert.assertNotNull(list);
        Assert.assertFalse(list.isEmpty());
        Assert.assertEquals(4, list.size());

        Assert.assertEquals(ProbeMovement.M, list.get(0));
        Assert.assertEquals(ProbeMovement.L, list.get(1));
        Assert.assertEquals(ProbeMovement.M, list.get(2));
        Assert.assertEquals(ProbeMovement.R, list.get(3));
    }
}
