package com.xerpa.mars.input;

import com.xerpa.mars.probe.Point;
import com.xerpa.mars.probe.ProbeDirection;
import com.xerpa.mars.probe.ProbePosition;
import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ProbePositionInputReaderTest {

    @Test
    public void createObject() throws Exception {
        Scanner scanner = new Scanner("2 2 N");

        ProbePositionInputReader probePositionReader = new ProbePositionInputReader(scanner);
        ProbePosition probePosition = probePositionReader.read();

        Assert.assertEquals(new Point(2, 2), probePosition.getPoint());
        Assert.assertEquals(ProbeDirection.N, probePosition.getDirection());
    }
}
