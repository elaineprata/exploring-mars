package com.xerpa.mars.input.parser;

import com.xerpa.mars.exception.InputValidationException;
import org.junit.Test;

public class InputValidatorTest {

    @Test
    public void validatePlateauInput() throws Exception {
        InputValidator.validatePlateauInput("5 5");
    }

    @Test(expected = InputValidationException.class)
    public void validatePlateauInput_exception() throws Exception {
        InputValidator.validatePlateauInput("dhakjdhshdk");
    }

    @Test
    public void validateProbePositionInput() throws Exception {
        InputValidator.validateProbePositionInput("0 0 N");
        InputValidator.validateProbePositionInput("0 0 E");
        InputValidator.validateProbePositionInput("0 0 S");
        InputValidator.validateProbePositionInput("0 0 W");
    }

    @Test(expected = InputValidationException.class)
    public void validateProbePositionInput_exception() throws Exception {
        InputValidator.validateProbePositionInput("0 0 D");
    }

    @Test
    public void validateProbeMovementsInput() throws Exception {
        InputValidator.validateProbeMovementsInput("MMLLRRMM");
        InputValidator.validateProbeMovementsInput("MLMRM");
    }

    @Test(expected = InputValidationException.class)
    public void validateProbeMovementsInput_exception() throws Exception {
        InputValidator.validateProbeMovementsInput("OPLS");
    }

}
