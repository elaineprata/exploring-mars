package com.xerpa.mars.input;

import com.xerpa.mars.probe.Plateau;
import com.xerpa.mars.probe.Point;
import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class PlateauInputReaderTest {

    @Test
    public void createObject() throws Exception {
        Scanner scanner = new Scanner("5 5");

        PlateauInputReader reader = new PlateauInputReader(scanner);
        Plateau plateau = reader.read();

        Assert.assertEquals(new Point(5, 5), plateau.getLimit());
    }
}