package com.xerpa.mars;

import com.xerpa.mars.config.AppProperties;
import com.xerpa.mars.controller.ProbeController;
import com.xerpa.mars.exception.InputValidationException;
import com.xerpa.mars.exception.InvalidInitialProbePositionsException;
import com.xerpa.mars.input.DataReader;
import com.xerpa.mars.output.ResultPrinter;
import com.xerpa.mars.probe.Plateau;
import com.xerpa.mars.probe.Probe;

public class Application {
	
	public static void main(String[] args) throws Exception {
		try {
			DataReader reader = new DataReader();
			Plateau p = reader.read();

			boolean collisionEnabled = AppProperties.getInstance().isCollisionEnabled();
			boolean stopAfterLimits = AppProperties.getInstance().isStopAfterReachLimits();

			ProbeController controller = new ProbeController(p, collisionEnabled, stopAfterLimits);
			controller.moveProbes();

			ResultPrinter.print(p);

		} catch (InputValidationException e) {
			System.out.println("Invalid input format: " + e.getMessage());
		} catch (InvalidInitialProbePositionsException e) {
			System.out.println("The initial position of probes is not valid: " + e.getMessage());
		}

	}
}