package com.xerpa.mars.controller;

import com.xerpa.mars.exception.InvalidInitialProbePositionsException;
import com.xerpa.mars.probe.*;

public class ProbeController {

    private Plateau plateau;

    private boolean collisionEnabled = false;
    private boolean stopAfterReachLimits = true;

    public ProbeController(Plateau plateau, boolean collisionEnabled, boolean stopAfterReachLimits) {
        this.plateau = plateau;
        this.collisionEnabled = collisionEnabled;
        this.stopAfterReachLimits = stopAfterReachLimits;
    }

    public void moveProbes() throws Exception {
        validateInitialProbePositions();

        for (Probe probe: plateau.getProbeList()) {
            moveProbe(probe);
        }
    }

    protected void validateInitialProbePositions() throws InvalidInitialProbePositionsException {
        for (Probe probe: plateau.getProbeList()) {
            if ((this.collisionEnabled)
                    && (this.plateau.isThereCollision(probe.getId(), probe.getPosition().getPoint()))) {
                throw new InvalidInitialProbePositionsException("There is a collision between 2 or more probes");
            }
            if (this.plateau.isOffLimits(probe.getPosition().getPoint())) {
                throw new InvalidInitialProbePositionsException("At least one of the probes is out off plateau limits");
            }
        }
    }

    protected void moveProbe(Probe probe) throws Exception {
        for (ProbeMovement pm: probe.getMovements()) {
            ProbePosition newPosition = probe.getPosition().calcNewPosition(pm);
            Point newPoint = newPosition.getPoint();

            if (this.plateau.isOffLimits(newPoint)) {
                if (this.stopAfterReachLimits) {
                    break;
                }
                continue;
            }

            if ((this.collisionEnabled)
                    && (this.plateau.isThereCollision(probe.getId(), newPoint))) {
                    break;
            }

            // we can move
            probe.setPosition(newPosition);
        }
    }
}
