package com.xerpa.mars.input;

import java.util.Scanner;

import com.xerpa.mars.exception.InputValidationException;
import com.xerpa.mars.input.parser.InputValidator;
import com.xerpa.mars.input.parser.PlateauParser;
import com.xerpa.mars.probe.Plateau;
import com.xerpa.mars.probe.Point;

public class PlateauInputReader extends BaseInputReader<Plateau> {

	public PlateauInputReader(Scanner scanner) {
		super(scanner);
	}

	@Override
	protected String getConsoleMessage() {
		return "Enter Plateau limits (X Y): ";
	}

	@Override
	protected void validateInput(String input) throws InputValidationException {
		InputValidator.validatePlateauInput(input);
	}

	@Override
	protected Plateau createObject(String input) {
		return PlateauParser.parse(input);
	}
}
