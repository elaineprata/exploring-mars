package com.xerpa.mars.input.parser;

import com.xerpa.mars.probe.Plateau;
import com.xerpa.mars.probe.Point;

public class PlateauParser {

    public static Plateau parse(String input) {

        String[] xy = input.split(" ");

        int x = Integer.valueOf(xy[0]);
        int y = Integer.valueOf(xy[1]);

        return new Plateau(new Point(x, y));
    }

}
