package com.xerpa.mars.input.parser;

import com.xerpa.mars.probe.Point;
import com.xerpa.mars.probe.ProbeDirection;
import com.xerpa.mars.probe.ProbePosition;

public class ProbePositionParser {

    public static ProbePosition parse(String input) {

        if ((input == null) || (input.isEmpty())) {
            return null;
        }

        // TODO handle invalid input
        String[] xyd = input.split(" ");

        // TODO handle invalid input
        int x = Integer.valueOf(xyd[0]);
        int y = Integer.valueOf(xyd[1]);

        ProbeDirection direction = ProbeDirection.valueOf(xyd[2]);

        return new ProbePosition(new Point(x, y), direction);
    }

}
