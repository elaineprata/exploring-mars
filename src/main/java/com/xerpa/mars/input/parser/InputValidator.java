package com.xerpa.mars.input.parser;

import com.xerpa.mars.exception.InputValidationException;

public class InputValidator {

    public static void validatePlateauInput(String input) throws InputValidationException {
        if (!input.matches("\\d+ \\d+")) {
            throw new InputValidationException("Invalid input for plateau limits: " + input);
        }
    }

    public static void validateProbePositionInput(String input) throws InputValidationException {
        if (!input.matches("\\d+ \\d+ [NESW]")) {
            throw new InputValidationException("Invalid input for probe position: " + input);
        }
    }

    public static void validateProbeMovementsInput(String input) throws InputValidationException {
        if (!input.matches("[LRM]+")) {
            throw new InputValidationException("Invalid input for probe movements: " + input);
        }
    }
}