package com.xerpa.mars.input.parser;

import com.xerpa.mars.probe.ProbeMovement;

import java.util.ArrayList;
import java.util.List;

public class ProbeMovementsParser {

    public static List<ProbeMovement> parseMovements(String input) {
        char[] ch = input.toCharArray();

        List<ProbeMovement> list = new ArrayList<>();
        for (char c: ch) {
            ProbeMovement pm = ProbeMovement.valueOf("" + c);
            list.add(pm);
        }

        return list;
    }
}
