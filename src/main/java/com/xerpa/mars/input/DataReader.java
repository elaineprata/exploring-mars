package com.xerpa.mars.input;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.xerpa.mars.exception.InputValidationException;
import com.xerpa.mars.probe.Plateau;
import com.xerpa.mars.probe.Probe;
import com.xerpa.mars.probe.ProbeMovement;
import com.xerpa.mars.probe.ProbePosition;

public class DataReader {
	
	public Plateau read() throws InputValidationException {
		try (Scanner scanner = new Scanner(System.in)) {
			
			PlateauInputReader reader = new PlateauInputReader(scanner);
			Plateau plateau = reader.read();
			
			List<Probe> probeList = new ArrayList<>();

			int i = 1;
			while (true) {
				ProbePositionInputReader probePositionReader = new ProbePositionInputReader(scanner);
				ProbePosition probePosition = probePositionReader.read();
				
				if (probePosition == null) {
					break;
				}
				
				ProbeMovementsInputReader pmReader = new ProbeMovementsInputReader(scanner);
				List<ProbeMovement> list = pmReader.read();
				
				Probe probe = new Probe(i, probePosition, list);
				probeList.add(probe);

				i++;
			}
			
			plateau.setProbeList(probeList);			
			return plateau;			
		}
	}

}
