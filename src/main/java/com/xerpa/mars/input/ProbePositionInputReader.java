package com.xerpa.mars.input;

import java.util.Scanner;

import com.xerpa.mars.exception.InputValidationException;
import com.xerpa.mars.input.parser.InputValidator;
import com.xerpa.mars.input.parser.ProbePositionParser;
import com.xerpa.mars.probe.ProbePosition;

public class ProbePositionInputReader extends BaseInputReader<ProbePosition> {

	public ProbePositionInputReader(Scanner scanner) {
		super(scanner);
	}

	@Override
	protected String getConsoleMessage() {
		return "Enter probe position (X Y D): ";
	}

	@Override
	protected void validateInput(String input) throws InputValidationException {
		if ((input != null) && (!input.isEmpty())) {
			InputValidator.validateProbePositionInput(input);
		}
	}

	@Override
	protected ProbePosition createObject(String input) {
		return ProbePositionParser.parse(input);
	}
}
