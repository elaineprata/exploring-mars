package com.xerpa.mars.input;

import com.xerpa.mars.exception.InputValidationException;

import java.util.Scanner;

public abstract class BaseInputReader<T> {
	
	Scanner scanner;
	
	public BaseInputReader(Scanner scanner) {
		this.scanner = scanner;
	}
	
	public T read() throws InputValidationException {
		String input = readInput();

		validateInput(input);
	    
	    T object = createObject(input);
	    
	    return object;
	}

	protected abstract void validateInput(String input) throws InputValidationException;
	
	protected abstract String getConsoleMessage();
	
	protected String readInput() {
		System.out.println(getConsoleMessage());
	    
	    String input = scanner.nextLine();
	    return input;	

	}
	
	protected abstract T createObject(String input);

}
