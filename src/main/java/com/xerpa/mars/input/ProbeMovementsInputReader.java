package com.xerpa.mars.input;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.xerpa.mars.exception.InputValidationException;
import com.xerpa.mars.input.parser.InputValidator;
import com.xerpa.mars.input.parser.ProbeMovementsParser;
import com.xerpa.mars.probe.ProbeMovement;

public class ProbeMovementsInputReader extends BaseInputReader<List<ProbeMovement>> {

	public ProbeMovementsInputReader(Scanner scanner) {
		super(scanner);
	}

	@Override
	protected String getConsoleMessage() {
		return "Enter probe movements (ex.: LMRM): ";
	}

	@Override
	protected void validateInput(String input) throws InputValidationException {
		InputValidator.validateProbeMovementsInput(input);
	}

	@Override
	protected List<ProbeMovement> createObject(String input) {
		return ProbeMovementsParser.parseMovements(input);
	}

}
