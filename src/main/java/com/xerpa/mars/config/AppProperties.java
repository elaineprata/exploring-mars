package com.xerpa.mars.config;

import java.io.FileInputStream;
import java.util.Properties;

public class AppProperties {

    private static AppProperties singleton = new AppProperties();

    private static Properties properties;

    private AppProperties() { }

    public static AppProperties getInstance() throws Exception {
        if (properties == null) {
            properties = new Properties();
            properties.load(new FileInputStream(Thread.currentThread().getContextClassLoader().getResource("application.properties").getPath()));
        }

        return singleton;
    }

    public boolean isCollisionEnabled() {
        String value = properties.getProperty("collision.enabled", "false");
        return Boolean.valueOf(value);
    }

    public boolean isStopAfterReachLimits() {
        String value = properties.getProperty("stop.after.reach.plateau.limit", "true");
        return Boolean.valueOf(value);
    }
}
