package com.xerpa.mars.exception;

public class InvalidInitialProbePositionsException extends Exception {

    public InvalidInitialProbePositionsException() {

    }

    public InvalidInitialProbePositionsException(String message) {
        super(message);
    }
}
