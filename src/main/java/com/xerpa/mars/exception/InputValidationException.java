package com.xerpa.mars.exception;

public class InputValidationException extends Exception {

    public InputValidationException() {}

    public InputValidationException(String message) {
        super(message);
    }
}
