package com.xerpa.mars.exception;

public class InvalidMovementException extends Exception {
	
	private static final long serialVersionUID = -5044249206288046163L;

	public InvalidMovementException() {
		
	}
	
	public InvalidMovementException(String message) {
		super(message);
	}
}