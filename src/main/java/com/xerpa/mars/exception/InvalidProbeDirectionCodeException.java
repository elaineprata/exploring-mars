package com.xerpa.mars.exception;

public class InvalidProbeDirectionCodeException extends Exception {

	private static final long serialVersionUID = 7137993016295276555L;

	public InvalidProbeDirectionCodeException() {
		
	}
	
	public InvalidProbeDirectionCodeException(String message) {
		super(message);
	}

}
