package com.xerpa.mars.probe;

import java.util.Objects;

public class ProbePosition {
	
	private Point point;
	private ProbeDirection direction;

	public ProbePosition(Point point, ProbeDirection direction) {
		super();
		this.point = point;
		this.direction = direction;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public ProbeDirection getDirection() {
		return direction;
	}

	public void setDirection(ProbeDirection direction) {
		this.direction = direction;
	}
	
	public ProbePosition calcNewPosition(ProbeMovement m) throws Exception {
		ProbeDirection newDirection = this.direction;
		Point newPoint = this.point;
		if (m.isDirectionModifier()) {
			newDirection = this.direction.changeDirection(m);
		} else {
			newPoint = this.point.add(this.direction.point);
		}
		ProbePosition newPosition = new ProbePosition(newPoint, newDirection);
		return newPosition;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ProbePosition that = (ProbePosition) o;
		return point.equals(that.point) &&
				direction == that.direction;
	}

	@Override
	public int hashCode() {
		return Objects.hash(point, direction);
	}

	@Override
	public String toString() {
		return "ProbePosition [point=" + point + ", direction=" + direction + "]";
	}

	public String outputString() {
		return point.getX() + " " + point.getY() + " " + direction;
	}

}