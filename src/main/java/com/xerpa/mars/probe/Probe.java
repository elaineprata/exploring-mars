package com.xerpa.mars.probe;

import java.util.List;
import java.util.Objects;

public class Probe {

	private int id;
	
	private ProbePosition position;
	
	private List<ProbeMovement> movements;

	public Probe(int id, ProbePosition position, List<ProbeMovement> movements) {
		super();
		this.id = id;
		this.position = position;
		this.movements = movements;
	}

	public int getId() { return id;	}

	public void setId(int id) { this.id = id; }

	public ProbePosition getPosition() {
		return position;
	}

	public void setPosition(ProbePosition position) {
		this.position = position;
	}

	public List<ProbeMovement> getMovements() {
		return movements;
	}

	public void setMovements(List<ProbeMovement> movements) {
		this.movements = movements;
	}

	@Override
	public String toString() {
		return "Probe [position=" + position + ", movements=" + movements + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Probe probe = (Probe) o;
		return id == probe.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}