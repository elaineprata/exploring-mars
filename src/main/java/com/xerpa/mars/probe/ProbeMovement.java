package com.xerpa.mars.probe;

public enum ProbeMovement {
	
	L(-1), R(1), M(0);
	
	int directionModifier;
	
	private ProbeMovement(int d) {
		this.directionModifier = d;
	}
	
	boolean isDirectionModifier() {
		return (this.directionModifier != 0);
	}
}
