package com.xerpa.mars.probe;

import com.xerpa.mars.exception.InvalidMovementException;
import com.xerpa.mars.exception.InvalidProbeDirectionCodeException;

public enum ProbeDirection {
	
	N(0, new Point(0, 1)), E(1, new Point(1, 0)), S(2, new Point(0, -1)), W(3, new Point(-1, 0));
	
	int code;
	Point point;
	
	private ProbeDirection(int code, Point point) {
		this.code = code;
		this.point = point;		
	}
	
	public ProbeDirection changeDirection(ProbeMovement m) throws InvalidMovementException, InvalidProbeDirectionCodeException {
		if (!m.isDirectionModifier()) {
			throw new InvalidMovementException("This movement is not a direction modifier");
		}
		
		int newCode = this.code + m.directionModifier;
		if (newCode < 0) {
			newCode += ProbeDirection.values().length;			
		} else {
			newCode = newCode % ProbeDirection.values().length;
		}
		
		return this.valueOf(newCode);
		
	}
	
	public ProbeDirection valueOf(int code) throws InvalidProbeDirectionCodeException {
		for (ProbeDirection pd: ProbeDirection.values()) {
			if (pd.code == code) {
				return pd;
			}
		}
		
		throw new InvalidProbeDirectionCodeException("Code not found: " + code);
	}
	

		
}
