package com.xerpa.mars.probe;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Plateau {

	private Point limit;
	
	private List<Probe> probeList = new ArrayList<>();
	
	public Plateau(Point limit) {
		this.limit = limit;
	}

	public Point getLimit() {
		return limit;
	}

	public void setLimit(Point limit) {
		this.limit = limit;
	}

	public List<Probe> getProbeList() {
		return probeList;
	}

	public void setProbeList(List<Probe> probeList) {
		this.probeList = probeList;
	}

	public void addProbe(Probe probe) { this.probeList.add(probe); }

	public boolean isOffLimits(Point point) {
		return ((point.getX() > this.limit.getX())
				|| (point.getY() > this.limit.getY())
				|| (point.getX() < 0)
				|| (point.getY() < 0));
	}

	public boolean isThereCollision(int probeId, Point point) {
		List<Probe> probeList = this.probeList
				.stream()
				.filter(p -> ((p.getPosition().getPoint().equals(point)) && (p.getId() != probeId)))
				.collect(Collectors.toList());

		return ((probeList != null) && (!probeList.isEmpty()));
	}

	@Override
	public String toString() {
		return "Plateau{" +
				"limit=" + limit +
				", probeList=" + probeList +
				'}';
	}
}