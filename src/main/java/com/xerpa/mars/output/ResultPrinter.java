package com.xerpa.mars.output;

import com.xerpa.mars.probe.Plateau;
import com.xerpa.mars.probe.Probe;

public class ResultPrinter {

    public static void print(Plateau p) {
        System.out.println("");

        for (Probe probe: p.getProbeList()) {
            System.out.println(probe.getPosition().outputString());
        }
    }
}
